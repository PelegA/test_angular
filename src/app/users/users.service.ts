import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
  
  //private _url= 'http://jsonplaceholder.typicode.com/users';
  userObservable;

  getUsers(){
   this.userObservable=this.af.database.list('/users');
   return this.userObservable;
  }
  addUser(user){
    this.userObservable.push(user);
  }
  updateUser(user){
    let userKey=user.$key;
    let userData={name:user.name,email:user.email};
    this.af.database.object('/users/'+userKey).update(userData);
  }
  deleteUser(user){
    let userKey=user.$key;
    this.af.database.object('/users/'+userKey).remove();
  }
  
  constructor(private af:AngularFire) { }

}
