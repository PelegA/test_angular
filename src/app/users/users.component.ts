import { Component, OnInit } from '@angular/core';
import { UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
   styles: [`
       .users li { cursor: default; }
       .users li:hover { background: #ecf0f1; }
        .list-group-item.active, 
        .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
         }
   `]
})
export class UsersComponent implements OnInit {
   
   users;
   currentUser;
   isLoading:Boolean=true;

  constructor(private _userService:UsersService) { }
  addUser(user){
    this._userService.addUser(user);
  }
  deleteUser(user){
    this._userService.deleteUser(user); 
   }
   updateUser(user){
     this._userService.updateUser(user);
   }
  ngOnInit() {
     this._userService.getUsers().subscribe(
       usersData=>{
         this.users=usersData; 
         this.isLoading=false
       });
   }
  select(user){
     this.currentUser=user;
   }
}
