import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';

import { PostsService} from './posts/posts.service';
import { UsersService} from './users/users.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostFormComponent } from './post-form/post-form.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { AngularFireModule } from 'angularfire2';

export const firebaseConfig={
    apiKey: "AIzaSyAuvjPKP-JT76Wi2k5LVh66KOOyPamaoiU",
    authDomain: "angularclass-5ef23.firebaseapp.com",
    databaseURL: "https://angularclass-5ef23.firebaseio.com",
    storageBucket: "angularclass-5ef23.appspot.com",
    messagingSenderId: "1053283773735"
}

const appRoutes:Routes= [
  {path: 'users', component:UsersComponent},
  {path: 'posts', component:PostsComponent},
  {path: '', component:SpinnerComponent},
  {path: '**', component:PageNotFoundComponent},
  ]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostFormComponent,
    UserFormComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostsService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
