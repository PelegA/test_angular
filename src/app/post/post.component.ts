import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Post} from './post';


@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  
  @Output() deleteEvent=new EventEmitter<Post>();
  @Output() editEvent=new EventEmitter<Post>();

  post:Post;
  originId:number;
  originTitle:string;
  isEdit:boolean=false;
  undoFlag:boolean=false;
  editButtonText="Edit";

  constructor(){
  }

  toggleEdit(){
    this.isEdit=!this.isEdit;
    if(this.isEdit){
       this.editButtonText="Edit"
       this.undoFlag=true   
    }
    else
      this.editEvent.emit(this.post)
      this.editButtonText="Save"
      this.undoFlag=false
  }


  sendDelete(){
    this.deleteEvent.emit(this.post);
  }
  toggleUndo(){
    this.undoFlag=true
    this.post.id=this.originId
    this.post.title=this.originTitle
    this.isEdit=false
    this.editButtonText="Edit"
  }
  ngOnInit() {
    this.originId=this.post.id;
    this.originTitle=this.post.title;
  }
  
}
