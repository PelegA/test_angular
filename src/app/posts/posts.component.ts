import { Component, OnInit } from '@angular/core';
import { PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styles: [`
       .list-group-item.active, 
       .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
        }
   `]
})
export class PostsComponent implements OnInit {

posts;
currentPost;
isLoading:boolean= true;

  constructor(private _postService:PostsService) { }
  
  addPost(post){
    this._postService.addPost(post);
  }
  updatePost(post){
    this._postService.updatePost(post);
  }
  deletePost(post){
    this._postService.deletePost(post);
  }
  ngOnInit() {
    this._postService.getPosts().subscribe(postsData=>{
      this.posts=postsData
      this.isLoading=false
    });
  }
  select(post){
    this.currentPost=post;
    console.log(this.posts);
  }
}
